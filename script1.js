$(document).ready(function(){
            $(".navbar a:contains('Ideas')").addClass("active");
        });

let lastScrollTop = 0;

        $(window).scroll(function() {
            let st = $(this).scrollTop();
            let header = $('.header');

            if (st > lastScrollTop) {
                // Scrolling down
                header.addClass('hidden');
            } else {
                // Scrolling up
                header.removeClass('hidden');
                if (st === 0) {
                    header.removeClass('transparent');
                } else {
                    header.addClass('transparent');
                }
            }

            lastScrollTop = st;
        });