var selectedCardCount = 10;
var sortOrder = "newest";


function updateCardOptions() {
    
    selectedCardCount = parseInt(document.getElementById('sort-page').value);

    sortOrder = document.getElementById('sort-by').value;

    
    var dataForNewCards = generateDataForNewCards();
    dataForNewCards = sortDataByDate(dataForNewCards, sortOrder);
    createCard(dataForNewCards);
}


function generateDataForNewCards() {
    var newData = [];
    for (var i = 0; i < selectedCardCount / 2; i++) {
        newData.push({
            "imageSrc": "images/artikel1.jpg",
            "date": "February 1, 2024",
            "title": "Manfaat Layanan Digital Agency Untuk Memajukan Bisnis"
        });
    }
    for (var i = 0; i < selectedCardCount / 2; i++) {
        newData.push({
            "imageSrc": "images/artikel2.jpg",
            "date": "February 1, 2023",
            "title": "Mockup Design: Pengertian, Manfaat, dan Contoh"
        });
    }
    return newData;
}


function sortDataByDate(data, order) {
    if (order === "newest") {
        return data.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
        });
    } else if (order === "oldest") {
        return data.sort(function (a, b) {
            return new Date(a.date) - new Date(b.date);
        });
    }
    return data;
}


function createCard(data) {
    var cardWrap = document.getElementById('cardWrap');
    cardWrap.innerHTML = ''; 

    
    data.forEach(function (item) {
        
        if (!item.title) {
            item.title = "No Title";
        }

        
        var card = document.createElement('div');
        card.className = 'sub-card';

        
        var imageContainer = document.createElement('div');
        imageContainer.className = 'image-container';
        var image = document.createElement('img');
        image.src = item.imageSrc;
        image.alt = 'Post Image';
        image.loading = 'lazy';
        imageContainer.appendChild(image);

        
        var datePost = document.createElement('div');
        datePost.className = 'date-post';
        datePost.innerHTML = '<p>' + item.date + '</p>';

        
        var title = document.createElement('div');
        title.className = 'title';
        title.innerHTML = '<h4>' + item.title + '</h4>';


        card.appendChild(imageContainer);
        card.appendChild(datePost);
        card.appendChild(title);

        
        cardWrap.appendChild(card);
    });
}


document.getElementById('sort-by').addEventListener('change', updateCardOptions);
document.getElementById('sort-page').addEventListener('change', updateCardOptions);


updateCardOptions();
